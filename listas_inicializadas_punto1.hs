{-
Alumno: Contreras Gustavo Esteban
MU:01558
-}

-- Archivo creado para inicializar las listas pedidas
-- en el inciso 2 del primer ejercicio

lista1 = [1..5]
lista2 = [1,3..9]
lista3 = ['a'..'f']