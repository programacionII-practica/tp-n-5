{-
Alumno: Contreras Gustavo Esteban
MU:01558
-}

-- Inciso i

{-
Defina una función potencia que dado dos números que representen la base y su potencia respectivamente, 
calcule su potencia utilizando solamente la multiplica-ción. 
En la definición de la función emplear guardas.
-}

potencia :: Int -> Int -> Int
potencia base exp
  | exp == 0 = 1
  | otherwise = base * potencia base (exp-1) 

-- Inciso ii

{-
Definir una función celsiusAFerenheit que dado un valor que represente una tem-peratura en °Celsius devuelva su equivalente en °Farenheit, 
teniendo en cuenta que la fórmula de conversión es de °F = °C * 1,800 + 32
-}

celsiusAFerenheit :: Float -> Float
celsiusAFerenheit c = c * 1.8 + 32

-- Inciso iii

{-
Definir la función concatenaLista tal que, dadas dos listas, 
lista1 y lista2 se ob-tenga una tercera concatenada con los elementos de ambas listas.
Por ejemplo: para las listas [2,3] [3,2,4,1]
concatenaLista [2,3] [3,2,4,1] = [2,3,3,2,4,1]
-}

concatenaLista :: [a] -> [a] -> [a]
concatenaLista lista1 lista2 = lista1 ++ lista2

-- Inciso iv

{-
Definir la función sumarCifra tal que dado un número natural devuelva la suma
de todas sus cifras. 
Por ejemplo, sumarCifra 23 = 5
-}

{-
  Crea una funcion "digitos" necesaria para la función sumarCifra
   Esta devuelve una lista de enteros cuyos elementos son los digitos de un numero x ingresado
   Se utiliza la función map, la cual recibe una función y una lista de elementos
   La función es (\d -> read [d] :: Int), esta lee un caracter y lo convierte en un entero
   La lista de elementos es (show x), convierte el argumento (en este caso un numero natural x) en una lista de caracteres

  La funcion sumarCifras se encarga de sumar todos los elementos de la lista entre si (en este caso, la suma de los digitos del numero ingresado)
-}

digitos:: Int -> [Int]
digitos x = map (\d -> read [d] :: Int) (show x)

sumarCifra :: Int -> Int
sumarCifra x = sum (digitos x) 

-- Inciso v

{-
Los factores propios de un número x son los divisores naturales de x estrictamente
menores a x. 
Un numero natural es perfecto i coincide con la suma de sus factores
propios 
por ejemplo 6 es perfecto ya que la suma de sus factores propios
1+2+3 = 6.
Escriba una función para comprobar si un número es perfecto. 
-}

esPerfecto :: Int -> Bool
esPerfecto x = x == sum[d | d <- [1..x-1] , mod x d == 0]

-- Inciso vi

{-
Definir por comprensión la función tal que la misma represente la lista de todos los números perfectos menores que un número dado. 
Por ejemplo: dado el número 30 me devolvería la lista [6,28].
-}

listaPerfectos :: Int -> [Int]
listaPerfectos n = [p | p <- [1..n] , esPerfecto p == True]

-- Inciso vii

{-
Definir la función Sumatoria tal que devuelva la suma de todos los números com-prendidos entre dos valores ingresados como argumentos. 
Definir dos versiones de la función empleando guardas y otra empleando el condicional if. en ambos casos hacer uso de la recursividad.
Por ejemplo (3,10) = 52
-}

sumatoriaGuarda :: Int -> Int -> Int
sumatoriaGuarda min max
  | min == max = min
  | otherwise = max + sumatoriaGuarda min (max-1)

sumatoriaCondicional :: Int -> Int -> Int
sumatoriaCondicional min max = if min == max then min else max + sumatoriaCondicional min (max-1)

-- Inciso viii

{-
Definir una función maximoDe2 que dados dos valores enteros retorne el mayor de ellos. 
Verifique que el tipo de la función creada compare con la definición 
y el tipo de la función max de la librería estándar de haskell
-}

maximoDe2 :: Ord a => a -> a-> a
maximoDe2 a b
  | a > b = a
  | b >= a = b

-- Inciso ix

{-
Defina una función maximode3 que dado 3 números enteros nos devuelva el ma-yor de ellos. 
Utilice el concepto de aplicación parcial empleando la función máxi-moDe2.m
-}

maximoDe3 a b c = maximoDe2 a (maximoDe2 b c)

-- Inciso x

{-
Dada una lista se pide escriba una función que obtenga la lista invertida, 
por ejem-plo, para la lista [2,3,6,8] se debe obtener [8,6,3,2]
-}

listaInvertida :: [a] -> [a]
listaInvertida [] = []
listaInvertida lista = last lista : listaInvertida(init lista)

-- Inciso xi

{-
Dadas dos listas de números enteros, 
se pide definir por comprensión el producto cada elemento de la primera lista por los elementos pares de la segunda lista.
Por ejemplo, para [5,7] y [8,6,7] se debe obtener [40,30,56,42].
-}

esPar :: Int -> Bool
esPar x = mod x 2 == 0

listaPares :: [Int] -> [Int]
listaPares [] = []
listaPares x = [n | n <- x , esPar n]

productoListaPares :: [Int] -> [Int] -> [Int]
productoListaPares lista1 lista2 = [x*y | x <- lista1 , y <- listaPares lista2]

-- Inciso xii

{-
Definir el operador !&& de tal forma que niegue la operación Y o and lógico. 
Este debe ser asociativo a la derecha y tener una orden de prioridad 3.
-}

infixr 3 !&&
(!&&) :: Bool -> Bool -> Bool
(!&&) a b = not (a && b)

-- Inciso xiii

{-
Compruebe los tipos de las funciones even y not. ¿Con estas funciones es posible aplicar el concepto de composición? En caso de ser posible escríbalo y compruebe su tipo
-}

-- Es posible aplicar el concepto
 
-- not.even, esta funcion devuelve el valor logico negado obtenido de comprobar si un numero es par (es decir, devuelve un booleano indicando si el numero es impar)

-- not.even :: Integral a => a -> bool  


-- Inciso xiv

{-
Definir una función promedio que obtenga el promedio de una lista de notas dadas. Se pide definir y utilizar las siguientes funciones:
mostrarNotas: muestra la lista con las notas
sumarNotas: devuelve la suma de las notas de la lista de notas
contarNotas: devuelve la cantidad de notas de la lista de notas.
Por ejemplo, para la lista [8, 6, 7,3, 9, 10] el promedio = 7,16
-}

mostrarNotas :: [Int] -> [Int]
mostrarNotas lista = lista

sumarNotas :: [Int] -> Int
sumarNotas lista = sum (mostrarNotas lista)

contarNotas :: [Int] -> Int
contarNotas lista = length (mostrarNotas lista)

promedioNotas :: [Int] -> Int
promedioNotas lista = (sumarNotas (mostrarNotas lista)) `div` (contarNotas(mostrarNotas lista))

-- Inciso xv

{-
Definir un tipo de dato mediante que represente los siguientes datos de un jugador: 
  nombre
  puestos en los que se desempeña 
  y cantidad de goles. 
Luego realizar funciones que nos permitan:
  Saber si jugo en más de una posición
  Agregar una nueva posición en la que se desempeña
  Agregar goles a su marca de goles
  Decir si es considerado un goleador, para ello debe superar los 20 en su marca.
  Definir si puede jugar en la liga de veteranos a la que puede participar si tiene entre 33 y 75 años.
-}

data Jugador = Jugador {
                        nombre :: String,
                        edad :: Int,
                        puestos :: [Int], 
                        cantGoles :: Int
                       } deriving (Show)

-- Jugadores inicializados
jugadorG = Jugador {nombre="Gustavo", edad=19, puestos=[11,9], cantGoles=21}
jugadorE = Jugador {nombre="Esteban", edad=40, puestos=[10,3], cantGoles=19}

masDeUnaPosicion :: Jugador -> Bool
masDeUnaPosicion jugador = (length (puestos jugador)) > 1

añadirPosicion :: Jugador -> Int -> [Int]
añadirPosicion jugador nuevaPosicion = puestos jugador ++ [nuevaPosicion]

añadirGoles :: Jugador -> Int -> Int
añadirGoles jugador nuevosGoles = cantGoles jugador + nuevosGoles

esGoleador :: Jugador -> String
esGoleador jugador
  | cantGoles jugador <= 20 = "No es goleador"
  | otherwise = "Goleador"

puedeJugarLigaVeteranos :: Jugador -> Bool  
puedeJugarLigaVeteranos jugador
  | edad jugador >= 33 && edad jugador <= 75 = True
  | otherwise = False

-- Inciso xvi

{-
Dados tres números enteros que representan el nivel de lago río en tres días consecutivos. 
Por ejemplo: en los días 1, 2 y 3, las mediciones son: 190 cm, 283 cm, y 294 cm. 
Se pide definir la función dispersión, que toma los tres valores y devuelve la diferencia entre el más alto y el más bajo. 
Ejemplo: dispersión de 190 283 294 es 104. Emplear la función maximode3 para que tome tres enteros.
-}

minimoDe2 :: Ord a => a -> a-> a
minimoDe2 a b
  | a > b = b
  | b >= a = a

minimoDe3 a b c = minimoDe2 a (minimoDe2 b c)

dispersion a b c = (maximoDe3 a b c) - (minimoDe3 a b c)

-- Inciso xvii

{-
Dado dos valores enteros se pide definir una función que nos devuelva ambos va-lores en una tupla binaria ordenada donde el primer componente sea el número ma-yor. 
Por ejemplo, para los números 2 5 se espera (5,2)
-}

tuplaDescendiente :: Int -> Int -> (Int,Int)
tuplaDescendiente a b = if maximoDe2 a b == a then (a,b)
                        else (b,a)

-- Inciso xviii

{-
Definir la función maximoEntero tal que dada una lista de n números devuelva el máximo valor de la lista. Por ejemplo,
MaximoEntero [2,4,1] = 4
-}

maximoLista :: [Int] -> Int
maximoLista [x] = x
maximoLista (x:y:xs) = maximoLista ( (if x >= y then x 
                                      else y):xs)

-- Inciso xix

{-
Definir un tipo de datos Figura y que permita calcular el perímetro de cada una de ellas. 
Las figuras a representar deben ser: triángulo, cuadrado, rectángulo y círculo.
Para toda otra opción debe emitir un mensaje de error
-}

data Figura = Triangulo {lado1 :: Float, lado2 :: Float, lado3 :: Float}
            | Cuadrado {lado :: Float}
            | Rectangulo {largo :: Float, ancho :: Float}
            | Circulo {radio :: Float} 
            | Extra {} deriving (Show)


perimetro :: Figura -> Float

perimetro (Circulo radio) = 2 * pi * radio
perimetro (Cuadrado lado) = 4 * lado
perimetro (Rectangulo largo ancho) = 2 * largo + 2 * ancho
perimetro (Triangulo lado1 lado2 lado3) = lado1 + lado2 + lado3
perimetro _ = error "Se ha ingresado un dato invalido. Asegurese de ingresar una figura geometrica"


-- Inciso xx

{-
Definir un tipo de dato Temp, que represente la temperatura tanto en grados centí-grados y en grados farenheit. Defina con ellos tres funciones que nos permitan:
 convertir de grados centígrados a Fahrenheit,
 convertir de grados Fahrenheit a centígrados
 determinar si el agua está congelada dado un valor en grados centígrados o Fahrenheit.
-}

data Temp = Celsius {gradosC :: Float}
          | Fahrenheit {gradosF :: Float} deriving (Show)

convertirCelsiusAFahrenheit :: Temp -> Float
convertirCelsiusAFahrenheit celsius =  (gradosC celsius) * 1.8 + 32
convertirFahrenheitACelsius :: Temp -> Float
convertirFahrenheitACelsius fahrenheit = (gradosF fahrenheit-32)/1.8

estaAguaCongelada :: Temp -> Bool
estaAguaCongelada (Celsius gradosC) = gradosC <= 0
estaAguaCongelada (Fahrenheit gradosF) = gradosF <= 32

-- Inciso xxi

{-
Realizar un programa que permita validar el número de cuil/cuit, teniendo en cuenta lo siguiente:
Tanto la CUIT (Clave Única de Identificación Tributaria) como el CUIL (Código Único de Identificación Laboral) constan de 11 números en las cuales se identifi-can tres partes separados por guion:
 el tipo,
 el número y
 el dígito verificador.
En donde para el siguiente ejemplo se toma como CUIT el número ##-12345678-X, donde ## es el tipo, 12345678 es el número de DNI o número de sociedad y X es el dígito verificador.
Los Tipos pueden ser:
 20, 23, 24 y 27 para Personas Físicas
 30, 33 y 34 para Empresas.
Valor de ##: se determina de la siguiente manera:
Hombre = 20, Mujer = 27 y Empresa o sociedad = 30
Digito Verificador: Se calcula usando el algoritmo Módulo 11. Para obtenerlo si no lo conocemos o si queremos calcularlo Se procede de la siguiente manera,
 tomar el número de 10 dígitos compuesto por los 2 primeros más los 8 dí-gitos siguientes y multiplicar cada dígito por los números que componen la serie numérica 2,3,4,5,6,7, de derecha a izquierda. Si se ha aplicado la se-rie hasta el 7 y quedan dígitos por multiplicar, se comienza la serie nueva-mente.
 A cada valor obtenido en el paso anterior, se lo suma para obtener una ex-presión (que llamaremos "valor 1".
 A este "valor 1", se le aplica el módulo 11, es decir se obtiene el resto de la división entera a 11.
 Se obtiene de esta forma un número (del 0 al 10) (que llamamos "valor 2"). Sacamos la diferencia entre 11 y el "valor 2", y obtenemos un valor com-prendido entre 1 y 11 (llamémosle "valor 3").
Si "valor 3"=11, el código verificador es cero.
Si "valor 3"=10, Se debe cambiar el tipo a 23 o 33 según se corresponda con persona física o empresa, y recalcular.
En cualquier otro caso, el código verificador es "valor 3".
Ejemplo numérico con un número de CUIT, que es 20-17254359-7. 2 0 1 7 2 5 4 3 5 9 x 5 4 3 2 7 6 5 4 3 2 ------------------------------------------------ 10 + 00 + 03 + 14 + 14 + 30 + 20 + 12 + 15 + 18 = 136 valor1 = 136 136 mod 11 = 4 valor2 = 4 11 - 4 = 7 valor3 = 7 => Código de verificación es siete.
-}

data Codigo = CUIL {tipo :: Int, numero :: Int, digitoVerificador :: Int}
            | CUIT {tipo :: Int, numero :: Int, digitoVerificador :: Int} deriving (Show)
data PropietarioCodigo = Hombre {codigo :: Codigo} 
                 | Mujer {codigo :: Codigo}
                 | Empresa {codigo :: Codigo} deriving (Show)


validarClave :: PropietarioCodigo -> Bool
validarClave propietarioCodigo
                           | cantidadDigitosClaveCorrecta(codigo propietarioCodigo) && (tipo (codigo propietarioCodigo)) == obtenerTipoCodigo(propietarioCodigo) && (digitoVerificador (codigo propietarioCodigo)) == modulo11 (propietarioCodigo) = True
                           | otherwise = False

cantidadDigitosClaveCorrecta :: Codigo -> Bool
cantidadDigitosClaveCorrecta codigo  
                                    |length (digitos(tipo codigo)) == 2 && length (digitos(tipo codigo)) == 8 && length (digitos(digitoVerificador codigo)) == 1 = True
                                    | otherwise = False
obtenerTipoCodigo :: PropietarioCodigo -> Int
obtenerTipoCodigo (Hombre codigo) = let validezTipo = (tipo codigo) == 20
                                        moduloIgual10 = modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (codigo))) == 10
                                    in 
                                        if validezTipo && moduloIgual10
                                        then 23
                                        else 20 
obtenerTipoCodigo (Mujer codigo) = let validezTipo = (tipo codigo) == 27
                                       moduloIgual10 = modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (codigo))) == 10
                                    in 
                                       if validezTipo && moduloIgual10
                                       then 24
                                       else 27 
obtenerTipoCodigo (Empresa codigo) = let validezTipo = (tipo codigo) == 30
                                         moduloIgual10 = modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (codigo))) == 10
                                     in
                                        if validezTipo && moduloIgual10
                                        then 33
                                        else 30                

modulo11 :: PropietarioCodigo -> Int
modulo11 (Hombre (CUIL tipo numero digitoVerificador)) = 
                                                        let tipoClasico = tipo == 20
                                                            moduloIgual10 = (modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIL tipo numero digitoVerificador))) == 10)
                                                        in
                                                            if tipoClasico && moduloIgual10
                                                            then modulo11(Hombre (CUIL 23 numero digitoVerificador))
                                                            else modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIL tipo numero digitoVerificador)))
                                                            
 
modulo11 (Hombre (CUIT tipo numero digitoVerificador)) = 
                                                        let tipoClasico = tipo == 20
                                                            moduloIgual10 = (modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIT tipo numero digitoVerificador))) == 10)
                                                        in
                                                            if tipoClasico && moduloIgual10
                                                            then modulo11(Hombre (CUIL 23 numero digitoVerificador))
                                                            else modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIT tipo numero digitoVerificador)))

modulo11 (Mujer (CUIL tipo numero digitoVerificador)) = 
                                                       let tipoClasico = tipo == 27
                                                           moduloIgual10 = (modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIL tipo numero digitoVerificador))) == 10)
                                                        in
                                                            if tipoClasico && moduloIgual10
                                                            then modulo11(Mujer (CUIL 24 numero digitoVerificador))
                                                            else modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIL tipo numero digitoVerificador)))

modulo11 (Mujer (CUIT tipo numero digitoVerificador)) = 
                                                       let tipoClasico = tipo == 27
                                                           moduloIgual10 = (modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIT tipo numero digitoVerificador))) == 10)
                                                        in
                                                           if tipoClasico && moduloIgual10
                                                           then modulo11(Mujer (CUIT 24 numero digitoVerificador))
                                                           else modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIT tipo numero digitoVerificador)))


modulo11 (Empresa (CUIL tipo numero digitoVerificador)) = 
                                                        let tipoClasico = tipo == 30
                                                            moduloIgual10 = (modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIL tipo numero digitoVerificador))) == 10)
                                                        in
                                                            if tipoClasico && moduloIgual10
                                                            then modulo11(Empresa (CUIL 33 numero digitoVerificador))
                                                            else modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIL tipo numero digitoVerificador)))


modulo11 (Empresa (CUIT tipo numero digitoVerificador)) = 
                                                        let tipoClasico = tipo == 30
                                                            moduloIgual10 = (modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIT tipo numero digitoVerificador))) == 10)
                                                        in
                                                            if tipoClasico && moduloIgual10
                                                            then modulo11(Empresa (CUIT 33 numero digitoVerificador))
                                                            else modulo11_paso3 (modulo11_paso2 (modulo11_paso1 (CUIT tipo numero digitoVerificador)))


modulo11_paso1 :: Codigo -> Int
modulo11_paso1 codigo = 
                        let listaASumar = multiplicarPorSerieNumerica (listaInvertida (concatenaLista (digitos (tipo codigo)) (digitos (numero codigo)) ))2

                        in 
                           sum listaASumar

modulo11_paso2 :: Int -> Int
modulo11_paso2 valor1 = valor1 `mod` 11

modulo11_paso3 :: Int -> Int
modulo11_paso3 valor2 = 
                        let resultado = 11 - valor2

                        in
                          if resultado == 11
                          then 0
                          else resultado
 

multiplicarPorSerieNumerica :: [Int] -> Int -> [Int]
multiplicarPorSerieNumerica [] valorSerie = []
multiplicarPorSerieNumerica lista valorSerie =
                                              let seRepite = valorSerie >= 8

                                              in
                                                if seRepite
                                                then head lista * (valorSerie-6) : multiplicarPorSerieNumerica (tail lista) (valorSerie-5) 
                                                else head lista * (valorSerie) : multiplicarPorSerieNumerica (tail lista) (valorSerie+1)
{-
Para probar. CUIL de personas reales. CUIT de la Universidad Nacional de Catamarca
cuilHombre = CUIL 20 43995185 3
cuilHombre2 = CUIL 20 43613164 0
cuilHombre3 = CUIL 20 39016583 9
cuilMujer = CUIL 27 39568513 4
cuitEmpresa = CUIT 30 64187093 1

hombre = Hombre cuilHombre 
hombre2 = Hombre cuilHombre2
hombre3 = Hombre cuilHombre3
mujer = Mujer cuilMujer
empresa = Empresa cuitEmpresa
-}                                                   